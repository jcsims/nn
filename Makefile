#the following code are micro 
###################################################################################

# my compiler g++ is now CC
NVCC=nvcc -m64 -O2 -arch=sm_20 -use_fast_math
		#-Xptxas -dlcm=cg # Disable L1 Cache
		#-Xptxas -dlcm=ca # Enable L1 Cache
		#-Xptxas -v       # Output Registers and Shared Memory information
		#-arch=sm_20      # Fermi Arch

CC=g++ -O2 -m64 -Wall
		#
		#-g 			#debuging only
		#-pg 			#profiling

# rules
SINGLE_THREADED = net
CUDA=net_cuda
#####################################################################################

# Creates the whole project 
###########################################
 
all: $(SINGLE_THREADED) $(CUDA) 

$(SINGLE_THREADED): net.cpp net.h
	$(CC) -o $(SINGLE_THREADED) net.cpp

$(CUDA): net_cuda.cu net_cuda.h
	$(NVCC) -o $(CUDA) net_cuda.cu
	
# The "clean" rule for this same Makefile could be used to delete the executable 
# file and all the object files from the directory. This will provide a fresh 
# compilation of the whole project. To do that, type: make clean
clean: 
	-rm -rf core *.o *~ "#"*"#" $(SINGLE_THREADED) $(CUDA) 
#############################################################################

# END OF makefile
