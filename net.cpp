// vim: set foldmethod=syntax :
//
// Neural Network with Backpropagation
// Written by Chris Sims

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include <cstdlib>
#include <ctime>
#include <cmath>

#include "net.h"

using namespace std;



int main() {

  //Init the runtime parameters
  int hiddenUnits = HIDDEN_UNITS;
  int epochs = EPOCHS;
  int outputUnits = OUTPUT_UNITS;
  double learningRate = LEARNING_RATE;

  Dataset dataset;
  struct timeval prog_start, prog_end;

  // Read the data
  gettimeofday(&prog_start, NULL);
  dataset = readData(DATA_FILE);

  cout << "Number of Training Observations: " << dataset.observations << endl;


  // Init the neural net
  Net net = initNet(dataset.dataDim, hiddenUnits, outputUnits);

  //Start the online training - update weights after testing each input
  double* errorDelta = new double[outputUnits];
  double* expected = new double[outputUnits];
  double deltaHidden;

  for (int i = 0; i < epochs; i++) {
    for (int j = 0; j < dataset.observations; ++j)
    {
      // Test the net on this observation
      calcOutput(dataset.data[j], net);

      // Only the output unit corresponding to the target number should be
      // outputting a number other than 0.1
      fill_n(expected, net.outputUnits, 0.1);
    
      // The output unit (0..9) has a target value of 0.9
      expected[(int) (dataset.data[j][dataset.dataDim - 1] + 0.5)] = 0.9; 

      // Calculate the error delta for each output unit
      for (int k = 0; k < net.outputUnits; ++k) {
        errorDelta[k] = net.output[k] * (1.0 - net.output[k]) * (expected[k]
            - net.output[k]);
      }

      // Calculate the error delta for each of the hidden units
      for (int k = 0; k < net.hiddenUnits; ++k)
      {
        deltaHidden = 0.0;
        for (int l = 0; l < net.outputUnits; ++l) {
          // The overall error delta for each hidden unit is proportional to
          // each output unit's error delta according to the weight connecting
          // the two
          deltaHidden += net.outWeights[l][k] * errorDelta[l];
          
          //Adjust each of the weights coming into the output nodes
          net.outWeights[l][k] += learningRate * errorDelta[l] * net.hiddenOutputs[k];
        }

        //Now calculate the error the current hidden unit, based on the
        //output errors
        deltaHidden = net.hiddenOutputs[k] * (1 - net.hiddenOutputs[k]) * deltaHidden;


        //Adjust each of the weights coming into the hidden units from the input
        //vector
        for (int l = 0; l < (dataset.dataDim - 1); ++l) {
          net.hiddenWeights[k][l] += learningRate * deltaHidden * dataset.data[j][l]; 
        }
      }
    }
    cout << "Epoch: " << i << " Expected: " << dataset.data[0][dataset.dataDim - 1] << endl;
    cout << "Outputs: " << endl;
    calcOutput(dataset.data[0], net);
    for (int m = 0; m < net.outputUnits; ++m) {
      printf("%10f", net.output[m]);
    }
    cout << endl;
    cout << "Hidden Units: " << endl;
    for (int m = 0; m < net.hiddenUnits; ++m) {
      printf("%10f", net.hiddenOutputs[m]);
    }
    cout << endl;

  }
  testNet(net, TEST_FILE);

  gettimeofday(&prog_end, NULL);
  printf("Total program run time: %.6f sec\n", timeDifference(&prog_start, &prog_end));
  return 0;

}

Net initNet(int dataDim, int hiddenUnits, int outputUnits) {

  Net net;
  //Ensure that we've got a relatively random seed
  srand(time(NULL));

  // Store the weights in the ANN
  // hiddenWeights: [hidden unit][input]
  net.hiddenWeights = new double*[hiddenUnits];
  for (int i = 0; i < hiddenUnits; ++i) {
    net.hiddenWeights[i] = new double[dataDim - 1];
  }
  // outWeights: [output][hidden unit]
  net.outWeights = new double*[outputUnits];
  for (int i = 0; i < outputUnits; ++i) {
    net.outWeights[i] = new double[hiddenUnits];
  }
  net.hiddenOutputs = new double[hiddenUnits];
  net.output = new double[outputUnits];
  
  fill_n(net.hiddenOutputs, hiddenUnits, 0);
  fill_n(net.output, outputUnits, 0);
  
  //Randomize each of the hidden unit weights
  for (int i = 0; i < hiddenUnits; ++i) {
    for (int j = 0; j < dataDim - 1; ++j) {
      net.hiddenWeights[i][j] = getRand();
    }
  }
 
  //Randomize the weights from the hidden units to the output nodes
  for (int i = 0; i < outputUnits; ++i) {
    for (int j = 0; j < hiddenUnits; ++j) {
      net.outWeights[i][j] = getRand();
    }
  }
  net.hiddenUnits = hiddenUnits;
  net.outputUnits = outputUnits;
  net.dataDim = dataDim;

  return net;
}

void calcOutput(double* input, Net &net) {

  // Ensure the outputs are zeroed out
  fill_n(net.output, net.outputUnits, 0.0);
  fill_n(net.hiddenOutputs, net.hiddenUnits, 0.0);

  // Calculate the input to each of the hidden units first
  for (int i = 0; i < net.hiddenUnits; ++i)
  {
    for (int j = 0; j < net.dataDim - 1; ++j)
    {
       net.hiddenOutputs[i] += input[j] * net.hiddenWeights[i][j];
    }
    net.hiddenOutputs[i] = activationFunction(net.hiddenOutputs[i]);
  }

  // Calculate each output neuron's output
  for (int i = 0; i < net.outputUnits; ++i)
  {
    for (int j = 0; j < net.hiddenUnits; ++j) {
      net.output[i] += net.hiddenOutputs[j] * net.outWeights[i][j];
    }
    net.output[i] = activationFunction(net.output[i]);
  }
}

void testNet(Net &net, string testFile) {
  // Bring in the test data
  
  Dataset dataset;
  dataset = readData(testFile); 

  //Test each data point
  double target, strongestValue;
  int strongest;
  int errors = 0;
  for (int i = 0; i < dataset.observations; ++i) {
    calcOutput(dataset.data[i], net);
    target = dataset.data[i][dataset.dataDim - 1];

    strongestValue = 0.0;
    strongest = 0;
    // Check to see if the strongest output corresponds to the target output
    for (int j = 0; j < net.outputUnits; ++j) {
      if (net.output[j] > strongestValue) {
        strongestValue = net.output[j];
        strongest = j;
      }
    }
    if (target != strongest) {
      errors++;
    }
  }

  cout << "Testing Observations: " << dataset.observations << endl;
  cout << "Percent error: " << ((double) errors / (double) dataset.observations) * 100.0 << "%" << endl;
}

// Typical logistic activation function
double activationFunction(double in) {
  return 1.0 / (1.0 + exp(-in));
}

/*********************************************************
 * Utility functions
 ********************************************************/
Dataset readData(string filename) {
  Dataset dataset;
  fstream file;

  file.open(filename.c_str(), ios::in);
  string in;
  vector<double> line;

  dataset.dataDim = 0;
  dataset.observations = 0;
  //Discard the headings
  getline(file,in);

  dataset.dataDim = split(in,',').size();
  while(getline(file, in)) {
    dataset.observations++;
  }

  dataset.data = new double*[dataset.observations];
  for (int i = 0; i < dataset.observations; ++i) {
    dataset.data[i] = new double[dataset.dataDim];
  }

  // Now that we've got the dimensions properly, reset so that we can read the
  // contents
  file.clear();
  file.seekg(0);

  //Discard the headings
  getline(file,in);

  for(int i = 0; i < dataset.observations; ++i) {
    getline(file, in);
    line = split(in,','); 
    copy(line.begin(), line.end(), dataset.data[i]);
  }
  scaleData(dataset);
  return dataset;
}

void scaleData(Dataset &dataset) {
  // General forumla used to scale:
  // for e in [a,b] to scale to [c,d]
  // calculate (e-a)/(b-a) * (d-c) + c
  //
  // So for the input data, this is simply dividing by the max value, 255
  for (int i = 0; i < dataset.observations; ++i) {
    for (int j = 0; j < (dataset.dataDim - 1); ++j) {
      dataset.data[i][j] = dataset.data[i][j] / 255.0;
    }
  }
}

// Generate a random number between -0.05 and 0.05
double getRand() {
  return ((((double) rand()) / (double) RAND_MAX) - 0.5) / 10.0;
}

vector<double> split(const string &s, char delim) {
  stringstream ss(s);
  string item;
  vector<double> elems;
  double number;
  while (getline(ss, item, delim)) {
    number = atof(item.c_str());
    elems.push_back(number);
  }
  return elems;
}
//
// The following method was used from Ron Duarte's example matrix multiplication code
float timeDifference (struct timeval * start, struct timeval * end) {
  struct timeval diff;

  diff.tv_sec = end->tv_sec - start->tv_sec;
  diff.tv_usec = end->tv_usec - start->tv_usec;

  return ((float)diff.tv_sec + (diff.tv_usec / 1.0e6));
}
