#ifndef NET_H
#define NET_H

#include <sys/time.h>

#define HIDDEN_UNITS 100
#define OUTPUT_UNITS 10
#define EPOCHS 100
#define LEARNING_RATE 0.1
#define DATA_FILE "train_med.csv"
#define TEST_FILE "test_small.csv"

typedef struct {
  double* output;
  double* hiddenOutputs;
  double** hiddenWeights;
  double** outWeights;
  int dataDim;
  int hiddenUnits;
  int outputUnits;
} Net;

typedef struct {
  double** data;
  int dataDim;
  int observations;
} Dataset;

// Prototypes
// Read the data file passed, populate the number of observations and the
// dimension of the data
Dataset readData(std::string filename);
// Split a delimited line into integer tokens
std::vector<double> split(const std::string &s, char delim);
// Generate a random number between -0.05 and 0.05
double getRand();
// Initialize the network weights
Net initNet(int dataDim, int hiddenUnits, int outputUnits);
// Given a single input, calculate the output of the network
void calcOutput(double* input, Net &net);
// Given the combined inputs to a node, calculate the node's output
// This is a simple sigmoid (or logistic) function
double activationFunction(double in);
// Test the network on a given data file
void testNet(Net &net, std::string testFile);
// Scale the input data
// In this case, we'll tailor towards the digit recognizer
void scaleData(Dataset &dataset);
// A function to compute execution times, from Ron Duarte's example code
float timeDifference (struct timeval * start, struct timeval * end);

#endif
