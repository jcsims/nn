// vim: set foldmethod=syntax :
//
// Neural Network with Backpropagation
// Written by Chris Sims

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include <cstdlib>
#include <ctime>
#include <cmath>

#include "net_cuda.h"

using namespace std;


int main() {

  //Init the runtime parameters
  int hiddenUnits = HIDDEN_UNITS;
  int epochs = EPOCHS;
  int outputUnits = OUTPUT_UNITS;
  float learningRate = LEARNING_RATE;

  Dataset dataset;
  struct timeval prog_start, prog_end, start, end;

  // Read the data
  gettimeofday(&prog_start, NULL);
  gettimeofday(&start, NULL);
  dataset = readData(DATA_FILE);
  gettimeofday(&end, NULL);
  printf("Time to load data set: %.6f sec\n", timeDifference(&start, &end));
  cout << "Number of Training Observations: " << dataset.observations << endl;

  // Init the neural net
  gettimeofday(&start, NULL);
  Net net = initNet(dataset.dataDim, hiddenUnits, outputUnits);
  gettimeofday(&end, NULL);
  printf("Time to initialize net: %.6f sec\n", timeDifference(&start, &end));

  // Set up the memory on the GPU for the first time
  float time;
  cudaEvent_t g_start, g_stop;
  cudaEventCreate(&g_start);
  cudaEventCreate(&g_stop);
  
  // Allocate the GPU memory we'll need
  float* g_input = NULL;
  float* g_hiddenWeights = NULL;
  float* g_hiddenOutputs= NULL;
  float* g_outWeights= NULL;
  float* g_output= NULL;
  float* g_outputErrorDelta= NULL;
  float* g_hiddenErrorDelta= NULL;
  float* g_expected= NULL;
  cudaEventRecord(g_start, 0);
  CUDA_SAFE_CALL(cudaMalloc((void **)&g_input, sizeof(float) * dataset.dataDim
        * dataset.observations));
  CUDA_SAFE_CALL(cudaMalloc((void **)&g_hiddenWeights, sizeof(float)
        * HIDDEN_UNITS * dataset.dataDim));
  CUDA_SAFE_CALL(cudaMalloc((void **)&g_hiddenOutputs, sizeof(float)
        * HIDDEN_UNITS));
  CUDA_SAFE_CALL(cudaMalloc((void **)&g_outWeights, sizeof(float)
        * HIDDEN_UNITS * OUTPUT_UNITS));
  CUDA_SAFE_CALL(cudaMalloc((void **)&g_output, sizeof(float) * OUTPUT_UNITS));
  CUDA_SAFE_CALL(cudaMalloc((void **)&g_outputErrorDelta, sizeof(float) * OUTPUT_UNITS));
  CUDA_SAFE_CALL(cudaMalloc((void **)&g_hiddenErrorDelta, sizeof(float) * HIDDEN_UNITS));
  CUDA_SAFE_CALL(cudaMalloc((void **)&g_expected, sizeof(float) * OUTPUT_UNITS));

  cudaThreadSynchronize();
  cudaEventRecord(g_stop, 0);
  cudaEventSynchronize(g_stop);
  cudaEventElapsedTime(&time, g_start, g_stop);
  printf("Time to allocate GPU memory: %.2f usec\n", time * 1e3);

  cudaEventRecord(g_start, 0);
  // Copy the weights to the GPU
  CUDA_SAFE_CALL(cudaMemcpy(g_hiddenWeights, net.hiddenWeights, HIDDEN_UNITS
        * dataset.dataDim * sizeof(float), cudaMemcpyHostToDevice));
  CUDA_SAFE_CALL(cudaMemcpy(g_outWeights, net.outWeights, HIDDEN_UNITS
        * OUTPUT_UNITS * sizeof(float), cudaMemcpyHostToDevice));
  // Copy the input data to the GPU
  CUDA_SAFE_CALL(cudaMemcpy(g_input, dataset.data, dataset.observations
        * dataset.dataDim * sizeof(float), cudaMemcpyHostToDevice));
  cudaEventRecord(g_stop, 0);
  cudaEventSynchronize(g_stop);
  cudaEventElapsedTime(&time, g_start, g_stop);
  printf("Time to copy data to GPU: %.2f usec\n", time * 1e3);

  // Start the training process
  cudaEventRecord(g_start, 0);
  cout << "Start training" << endl;
  for (int i = 0; i < epochs; i++) {
    for (int j = 0; j < dataset.observations; ++j)
    {
      // Test the net on this observation
      calcOutput(g_input, j, net, g_hiddenWeights, g_hiddenOutputs,
          g_outWeights, g_output);

      CUDA_SAFE_CALL(cudaMemset(g_hiddenErrorDelta, 0, net.hiddenUnits * sizeof(float)));
      CUDA_SAFE_CALL(cudaMemset(g_outputErrorDelta, 0, net.outputUnits * sizeof(float)));

      // Ensure that the expected values are populated for this observation
      populateExpectedValue<<<1,1>>>(g_input, g_expected,  net.dataDim, j,
          net.outputUnits);
      // Calculate the error delta for each output unit
      calcOutputErrorDelta<<<1,HIDDEN_UNITS>>>(g_expected, g_outputErrorDelta, g_output);
      // Calculate the error delta for each of the hidden units
      calcHiddenErrorDelta<<<BLOCKS,BLOCKSIZE>>>(g_outWeights, g_hiddenOutputs,
          g_outputErrorDelta, g_hiddenErrorDelta,  net.outputUnits);
      //Update the weights on the output nodes
      updateNodeWeights<<<1,HIDDEN_UNITS>>>(g_outputErrorDelta, g_outWeights,
          g_hiddenOutputs, net.hiddenUnits, 0, learningRate);
      //Update the weights on the hidden nodes
      updateNodeWeights<<<BLOCKS,BLOCKSIZE>>>(g_hiddenErrorDelta, g_hiddenWeights,
          g_input, net.dataDim, j, learningRate);
      
    }
    cout << "Epoch: " << i << endl;
  }
  cudaEventRecord(g_stop, 0);
  cudaEventSynchronize(g_stop);
  cudaEventElapsedTime(&time, g_start, g_stop);
  printf("Time to train net: %.2f usec\n", time * 1e3);

  gettimeofday(&start, NULL);
  testNet(g_input, g_hiddenWeights, g_hiddenOutputs, g_outWeights, g_output,
      net, TEST_FILE);
  gettimeofday(&end, NULL);
  printf("Time to test net: %.6f sec\n", timeDifference(&start, &end));

  CUDA_SAFE_CALL(cudaFree(g_output));
  CUDA_SAFE_CALL(cudaFree(g_outWeights));
  CUDA_SAFE_CALL(cudaFree(g_input));
  CUDA_SAFE_CALL(cudaFree(g_hiddenWeights));
  CUDA_SAFE_CALL(cudaFree(g_hiddenOutputs));
  CUDA_SAFE_CALL(cudaFree(g_hiddenErrorDelta));
  CUDA_SAFE_CALL(cudaFree(g_outputErrorDelta));

  gettimeofday(&prog_end, NULL);
  printf("Total program run time: %.6f sec\n", timeDifference(&prog_start, &prog_end));
  return 0;

}

Net initNet(int dataDim, int hiddenUnits, int outputUnits) {

  Net net;
  //Ensure that we've got a relatively random seed
  srand(time(NULL));

  // Store the weights in the ANN
  // hiddenWeights: [hidden unit][input]
  net.hiddenWeights = new float[hiddenUnits * (dataDim - 1)];
  // outWeights: [output][hidden unit]
  net.outWeights = new float[outputUnits * hiddenUnits];
  net.hiddenOutputs = new float[hiddenUnits];
  net.output = new float[outputUnits];
  
  fill_n(net.hiddenOutputs, hiddenUnits, 0);
  fill_n(net.output, outputUnits, 0);
  
  //Randomize each of the hidden unit weights
  for (int i = 0; i < hiddenUnits; ++i) {
    for (int j = 0; j < dataDim - 1; ++j) {
      net.hiddenWeights[i * (dataDim - 1) + j] = getRand();
    }
  }
 
  //Randomize the weights from the hidden units to the output nodes
  for (int i = 0; i < outputUnits; ++i) {
    for (int j = 0; j < hiddenUnits; ++j) {
      net.outWeights[i * hiddenUnits + j] = getRand();
    }
  }
  net.hiddenUnits = hiddenUnits;
  net.outputUnits = outputUnits;
  net.dataDim = dataDim;

  return net;
}

void calcOutput(float* input, int observation, Net &net, float* hiddenWeights,
    float* hiddenOutputs, float* outWeights, float* output) {

  // Ensure the outputs are zeroed out
  CUDA_SAFE_CALL(cudaMemset(hiddenOutputs, 0, net.hiddenUnits * sizeof(float)));
  // Calculate the input to each of the hidden units first
  calcNodeOutput<<<BLOCKS,BLOCKSIZE>>>(input, hiddenWeights, hiddenOutputs,
      observation, net.dataDim);
  // While that calculation is running, zero out the outputs
  CUDA_SAFE_CALL(cudaMemset(output, 0, net.outputUnits * sizeof(float)));
  // Make sure the copying and previous calculations are complete, then start
  // calculating the output layer 
  calcNodeOutput<<<1,net.outputUnits>>>(hiddenOutputs, outWeights, output, 0,
      net.hiddenUnits);
}

// Calculate the output for a single node
__global__ void calcNodeOutput(float* input, float* weights, float* output,
    int observation, int inputDim) {
  int id = blockIdx.x * BLOCKSIZE + threadIdx.x;
  for (int i = 0; i <  inputDim; ++i) {
    output[id] += input[observation * inputDim + i] * weights[id * inputDim + i];
  }
  output[id] = 1.0 / (1.0 + expf(-output[id]));
}

__global__ void calcOutputErrorDelta(float* expected, float* outputErrorDelta,
    float* output) {
  int id = threadIdx.x;
  outputErrorDelta[id] = output[id] * (1.0 - output[id]) * (expected[id] - output[id]);
}

__global__ void calcHiddenErrorDelta(float* outWeights, float* hiddenOutputs,
    float* outputErrorDelta, float* hiddenErrorDelta, int outputUnits) {
  int id = blockIdx.x * BLOCKSIZE + threadIdx.x;
  float delta = 0.0;
  for (int i = 0; i < outputUnits; ++i) {
    delta += outWeights[i * outputUnits + id] * outputErrorDelta[i];
  }
  hiddenErrorDelta[id] = hiddenOutputs[id] * (1 - hiddenOutputs[id]) * delta;
}

__global__ void updateNodeWeights(float* errorDeltas, float* weights, float*
    input, int inputDim, int observation, float learningRate) {
  int id = blockIdx.x * BLOCKSIZE + threadIdx.x;
  for (int i = 0; i < inputDim; ++i) {
    weights[id * inputDim + i] += learningRate * errorDeltas[id]
      * input[observation * inputDim + i];
  }
}

__global__ void populateExpectedValue(float* input, float* expected, int
    inputDim, int observation, int outputDim) {

  int expectedValue = (int) input[observation * inputDim + inputDim - 1] + 0.5;
  // Only the output unit corresponding to the target number should be
  // outputting a number other than 0.1
  for (int i = 0; i < outputDim; ++i) {
    if (i == expectedValue)
      expected[i] = 0.9;
    else
      expected[i] = 0.1;
  }
}
void testNet(float* input, float* hiddenWeights, float* hiddenOutputs, float*
    outWeights, float* output, Net &net, string testFile) {
  // Bring in the test data
  
  Dataset dataset;
  dataset = readData(testFile); 

  CUDA_SAFE_CALL(cudaMemcpy(input, dataset.data, dataset.dataDim
        * dataset.observations * sizeof(float), cudaMemcpyHostToDevice));
  //Test each data point
  float target, strongestValue;
  int strongest;
  int errors = 0;
  for (int i = 0; i < dataset.observations; ++i) {
    calcOutput(input, i, net, hiddenWeights, hiddenOutputs, outWeights, output);
    target = dataset.data[i * dataset.dataDim + dataset.dataDim - 1];
    CUDA_SAFE_CALL(cudaMemcpy(net.output, output, OUTPUT_UNITS * sizeof(float),
          cudaMemcpyDeviceToHost));

    strongestValue = 0.0;
    strongest = 0;
    // Check to see if the strongest output corresponds to the target output
    for (int j = 0; j < net.outputUnits; ++j) {
      if (net.output[j] > strongestValue) {
        strongestValue = net.output[j];
        strongest = j;
      }
    }
    if (target != strongest) {
      errors++;
    }
  }

  cout << "Testing Observations: " << dataset.observations << endl;
  cout << "Percent error: " << ((float) errors / (float) dataset.observations) * 100.0 << "%" << endl;
}

// Typical logistic activation function
float activationFunction(float in) {
  return 1.0 / (1.0 + exp(-in));
}

/*********************************************************
 * Utility functions
 ********************************************************/
Dataset readData(string filename) {
  Dataset dataset;
  fstream file;

  file.open(filename.c_str(), ios::in);
  string in;
  vector<float> line;

  dataset.dataDim = 0;
  dataset.observations = 0;
  //Discard the headings
  getline(file,in);

  dataset.dataDim = split(in,',').size();
  while(getline(file, in)) {
    dataset.observations++;
  }

  dataset.data = new float[dataset.observations * dataset.dataDim];

  // Now that we've got the dimensions properly, reset so that we can read the
  // contents
  file.clear();
  file.seekg(0);

  //Discard the headings
  getline(file,in);

  for(int i = 0; i < dataset.observations; ++i) {
    getline(file, in);
    line = split(in,','); 
    for (int j = 0; j < dataset.dataDim; ++j) {
      dataset.data[i * dataset.dataDim + j] = line[j];
    }
  }
  scaleData(dataset);
  return dataset;
}

void scaleData(Dataset &dataset) {
  // General forumla used to scale:
  // for e in [a,b] to scale to [c,d]
  // calculate (e-a)/(b-a) * (d-c) + c
  //
  // So for the input data, this is simply dividing by the max value, 255
  for (int i = 0; i < dataset.observations; ++i) {
    for (int j = 0; j < (dataset.dataDim - 1); ++j) {
      dataset.data[i * dataset.dataDim + j] = dataset.data[i * dataset.dataDim + j] / 255.0;
    }
  }
}

// Generate a random number between -0.05 and 0.05
float getRand() {
  return ((((float) rand()) / (float) RAND_MAX) - 0.5) / 10.0;
}

vector<float> split(const string &s, char delim) {
  stringstream ss(s);
  string item;
  vector<float> elems;
  float number;
  while (getline(ss, item, delim)) {
    number = atof(item.c_str());
    elems.push_back(number);
  }
  return elems;
}

// The following method was used from Ron Duarte's example matrix multiplication code
float timeDifference (struct timeval * start, struct timeval * end) {
  struct timeval diff;

  diff.tv_sec = end->tv_sec - start->tv_sec;
  diff.tv_usec = end->tv_usec - start->tv_usec;

  return ((float)diff.tv_sec + (diff.tv_usec / 1.0e6));
}
