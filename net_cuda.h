#ifndef NET_CUDA_H
#define NET_CUDA_H

#include <sys/time.h>
#include <cuda.h>

#define CUDA_SAFE_CALL( error) \
  if (error != cudaSuccess) { \
    printf("Error = %s\n\n", cudaGetErrorString(error)); \
    exit(EXIT_FAILURE); \
  }

#define HIDDEN_UNITS 100
#define OUTPUT_UNITS 10
#define EPOCHS 100
#define LEARNING_RATE 0.1
#define BLOCKSIZE 10
#define BLOCKS 10
#define DATA_FILE "train_med.csv"
#define TEST_FILE "test_small.csv"

typedef struct {
  float* output;
  float* hiddenOutputs;
  float* hiddenWeights;
  float* outWeights;
  int dataDim;
  int hiddenUnits;
  int outputUnits;
} Net;

typedef struct {
  float* data;
  int dataDim;
  int observations;
} Dataset;

// Kernel functions
__global__ void calcNodeOutput(float* input, float* weights, float* output,
    int observation, int inputDim);
__global__ void calcOutputErrorDelta(float* expected, float* outputErrorDelta,
    float* output);
__global__ void calcHiddenErrorDelta(float* outWeights, float* hiddenOutputs,
    float* outputErrorDelta, float* hiddenErrorDelta, int outputUnits);
__global__ void updateNodeWeights(float* errorDeltas, float* weights, float*
    input, int inputDim, int observation, float learningRate);
__global__ void populateExpectedValue(float* input, float* expected, int
    inputDim, int observation, int outputDim);
// Prototypes
// Read the data file passed, populate the number of observations and the
// dimension of the data
Dataset readData(std::string filename);
// Split a delimited line into integer tokens
std::vector<float> split(const std::string &s, char delim);
// Generate a random number between -0.05 and 0.05
float getRand();
// Initialize the network weights
Net initNet(int dataDim, int hiddenUnits, int outputUnits);
// Given a single input, calculate the output of the network
void calcOutput(float* input, int observation, Net &net, float* hiddenWeights,
    float* hiddenOutputs, float* outWeights, float* output);
// Given the combined inputs to a node, calculate the node's output
// This is a simple sigmoid (or logistic) function
float activationFunction(float in);
// Test the network on a given data file
void testNet(float* input, float* hiddenWeights, float* hiddenOutputs, float*
    outWeights, float* output, Net &net, std::string testFile);
// Scale the input data
// In this case, we'll tailor towards the digit recognizer
void scaleData(Dataset &dataset);
// A function to compute execution times, from Ron Duarte's example code
float timeDifference (struct timeval * start, struct timeval * end);

#endif
