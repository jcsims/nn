// vim: set foldmethod=syntax :
//
// Neural Network with Backpropagation
// Written by Chris Sims

#include <iostream>
#include <chrono>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <random>
#include <thread>

#include <cstdlib>
#include <cmath>

#include "net_threads.h"

using namespace std;

int main() {

  //Init the runtime parameters
  int hiddenUnits = HIDDEN_UNITS;
  int epochs = EPOCHS;
  int outputUnits = OUTPUT_UNITS;
  float learningRate = LEARNING_RATE;

  Dataset dataset;

  // Read the data
  auto start = chrono::high_resolution_clock::now();
  dataset = readData(DATA_FILE);
  auto finish = chrono::high_resolution_clock::now();
  cout << "Time to load dataset: ";
  cout << chrono::duration_cast<chrono::microseconds>(finish-start).count() << "us\n";

  cout << "Number of Training Observations: " << dataset.observations << endl;


  Net nets[THREADS];
  // Init the neural nets
  for (int i = 0; i < THREADS; ++i) {
    nets[i] = initNet(dataset.dataDim, hiddenUnits, outputUnits);
  }

  start = chrono::high_resolution_clock::now();

  thread* workers = new thread[THREADS];

  // Start the workers!
  for (int i = 0; i < THREADS; ++i) {
    workers[i] = thread(trainNet, ref(nets[i]), ref(dataset), epochs, learningRate, i); 
  }

  // Join the threads to wait for completion
  for (int i = 0; i < THREADS; ++i) {
    workers[i].join();
  }

  combineNets(nets);
  
  finish = chrono::high_resolution_clock::now();
  cout << "Time to train net: ";
  cout << chrono::duration_cast<chrono::microseconds>(finish-start).count() << "us\n";
  testNet(nets[0], TEST_FILE);

  return 0;

}

Net initNet(int dataDim, int hiddenUnits, int outputUnits) {

  Net net;
  //Ensure that we've got a relatively random seed
  srand(time(NULL));

  // Store the weights in the ANN
  // hiddenWeights: [hidden unit][input]
  net.hiddenWeights = new float*[hiddenUnits];
  for (int i = 0; i < hiddenUnits; ++i) {
    net.hiddenWeights[i] = new float[dataDim - 1];
  }
  // outWeights: [output][hidden unit]
  net.outWeights = new float*[outputUnits];
  for (int i = 0; i < outputUnits; ++i) {
    net.outWeights[i] = new float[hiddenUnits];
  }
  net.hiddenOutputs = new float[hiddenUnits];
  net.output = new float[outputUnits];
  
  fill_n(net.hiddenOutputs, hiddenUnits, 0);
  fill_n(net.output, outputUnits, 0);
  
  //Randomize each of the hidden unit weights
  for (int i = 0; i < hiddenUnits; ++i) {
    for (int j = 0; j < dataDim - 1; ++j) {
      net.hiddenWeights[i][j] = getRand();
    }
  }
 
  //Randomize the weights from the hidden units to the output nodes
  for (int i = 0; i < outputUnits; ++i) {
    for (int j = 0; j < hiddenUnits; ++j) {
      net.outWeights[i][j] = getRand();
    }
  }
  net.hiddenUnits = hiddenUnits;
  net.outputUnits = outputUnits;
  net.dataDim = dataDim;

  return net;
}

void trainNet(Net &net, Dataset &dataset, int epochs, float learningRate, int id) {

  cout << id << " thread starting" << endl;

  int dataPoints = dataset.observations / THREADS;
  int start = id * dataPoints;
  int end = start + dataPoints;
  //Start the online training - update weights after testing each input
  float* errorDelta = new float[net.outputUnits];
  float* expected = new float[net.outputUnits];
  float deltaHidden;

  for (int i = 0; i < epochs; i++) {
    for (int j = start; j < end; ++j)
    {
      // Test the net on this observation
      calcOutput(dataset.data[j], net);

      // Only the output unit corresponding to the target number should be
      // outputting a number other than 0.1
      fill_n(expected, net.outputUnits, 0.1);
    
      // The output unit (0..9) has a target value of 0.9
      expected[(int) (dataset.data[j][dataset.dataDim - 1] + 0.5)] = 0.9; 

      // Calculate the error delta for each output unit
      for (int k = 0; k < net.outputUnits; ++k) {
        errorDelta[k] = net.output[k] * (1.0 - net.output[k]) * (expected[k]
            - net.output[k]);
      }

      // Calculate the error delta for each of the hidden units
      for (int k = 0; k < net.hiddenUnits; ++k)
      {
        deltaHidden = 0.0;
        for (int l = 0; l < net.outputUnits; ++l) {
          // The overall error delta for each hidden unit is proportional to
          // each output unit's error delta according to the weight connecting
          // the two
          deltaHidden += net.outWeights[l][k] * errorDelta[l];
          
          //Adjust each of the weights coming into the output nodes
          net.outWeights[l][k] += learningRate * errorDelta[l] * net.hiddenOutputs[k];
        }

        //Now calculate the error the current hidden unit, based on the
        //output errors
        deltaHidden = net.hiddenOutputs[k] * (1 - net.hiddenOutputs[k]) * deltaHidden;


        //Adjust each of the weights coming into the hidden units from the input
        //vector
        for (int l = 0; l < (dataset.dataDim - 1); ++l) {
          net.hiddenWeights[k][l] += learningRate * deltaHidden * dataset.data[j][l]; 
        }
      }
    }
    /* cout << "Epoch: " << i << " Expected: " << dataset.data[0][dataset.dataDim - 1] << endl; */
    /* cout << "Outputs: " << endl; */
    /* calcOutput(dataset.data[0], net); */
    /* for (int m = 0; m < net.outputUnits; ++m) { */
    /*   printf("%10f", net.output[m]); */
    /* } */
    /* cout << endl; */
    /* cout << "Hidden Units: " << endl; */
    /* for (int m = 0; m < net.hiddenUnits; ++m) { */
    /*   printf("%10f", net.hiddenOutputs[m]); */
    /* } */
    /* cout << endl; */
    cout << "Thread " << id << ", Epoch " << i << endl;
  }
  cout << id << " thread completed" << endl;
}


void calcOutput(float* input, Net &net) {

  // Ensure the outputs are zeroed out
  fill_n(net.output, net.outputUnits, 0.0);
  fill_n(net.hiddenOutputs, net.hiddenUnits, 0.0);

  // Calculate the input to each of the hidden units first
  for (int i = 0; i < net.hiddenUnits; ++i)
  {
    for (int j = 0; j < net.dataDim - 1; ++j)
    {
       net.hiddenOutputs[i] += input[j] * net.hiddenWeights[i][j];
    }
    net.hiddenOutputs[i] = activationFunction(net.hiddenOutputs[i]);
  }

  // Calculate each output neuron's output
  for (int i = 0; i < net.outputUnits; ++i)
  {
    for (int j = 0; j < net.hiddenUnits; ++j) {
      net.output[i] += net.hiddenOutputs[j] * net.outWeights[i][j];
    }
    net.output[i] = activationFunction(net.output[i]);
  }
}

void combineNets(Net* nets) {
  
  auto start = chrono::high_resolution_clock::now();
  // We'll need to combine the hidden weights and the output weights
  // A simple mean value is used
  for (int i = 0; i < nets[0].hiddenUnits; ++i) {
    for (int j = 0; j < nets[0].outputUnits; ++j) {
      for (int k = 1; k < THREADS; ++k) {
        nets[0].outWeights[j][i] += nets[k].outWeights[j][i];
      }
      nets[0].outWeights[j][i] /= THREADS;
    }
    for (int j = 0; j < nets[0].dataDim; ++j) {
      for (int k = 1; k < THREADS; ++k) {
        nets[0].hiddenWeights[i][j] += nets[k].hiddenWeights[i][j];
      }
      nets[0].hiddenWeights[i][j] /= THREADS;
    }
  }
  auto finish = chrono::high_resolution_clock::now();
  cout << "Time to combine nets: ";
  cout << chrono::duration_cast<chrono::microseconds>(finish-start).count() << "us\n";
}


void testNet(Net &net, string testFile) {
  // Bring in the test data
  
  auto start = chrono::high_resolution_clock::now();
  Dataset dataset;
  dataset = readData(testFile); 

  //Test each data point
  float target, strongestValue;
  int strongest;
  int errors = 0;
  for (int i = 0; i < dataset.observations; ++i) {
    calcOutput(dataset.data[i], net);
    target = dataset.data[i][dataset.dataDim - 1];

    strongestValue = 0.0;
    strongest = 0;
    // Check to see if the strongest output corresponds to the target output
    for (int j = 0; j < net.outputUnits; ++j) {
      if (net.output[j] > strongestValue) {
        strongestValue = net.output[j];
        strongest = j;
      }
    }
    if (target != strongest) {
      errors++;
    }
  }

  auto finish = chrono::high_resolution_clock::now();
  cout << "Time to test net: ";
  cout << chrono::duration_cast<chrono::microseconds>(finish-start).count() << "us\n";
  cout << "Testing Observations: " << dataset.observations << endl;
  cout << "Percent error: " << ((float) errors / (float) dataset.observations) * 100.0 << "%" << endl;
}

// Typical logistic activation function
float activationFunction(float in) {
  return 1.0 / (1.0 + exp(-in));
}

/*********************************************************
 * Utility functions
 ********************************************************/
Dataset readData(string filename) {
  Dataset dataset;
  fstream file;

  file.open(filename, ios::in);
  string in;
  vector<float> line;

  dataset.dataDim = 0;
  dataset.observations = 0;
  //Discard the headings
  getline(file,in);

  dataset.dataDim = split(in,',').size();
  while(getline(file, in)) {
    dataset.observations++;
  }

  dataset.data = new float*[dataset.observations];
  for (int i = 0; i < dataset.observations; ++i) {
    dataset.data[i] = new float[dataset.dataDim];
  }

  // Now that we've got the dimensions properly, reset so that we can read the
  // contents
  file.clear();
  file.seekg(0);

  //Discard the headings
  getline(file,in);

  for(int i = 0; i < dataset.observations; ++i) {
    getline(file, in);
    line = split(in,','); 
    copy(line.begin(), line.end(), dataset.data[i]);
  }
  scaleData(dataset);
  return dataset;
}

void scaleData(Dataset &dataset) {
  // General forumla used to scale:
  // for e in [a,b] to scale to [c,d]
  // calculate (e-a)/(b-a) * (d-c) + c
  //
  // So for the input data, this is simply dividing by the max value, 255
  for (int i = 0; i < dataset.observations; ++i) {
    for (int j = 0; j < (dataset.dataDim - 1); ++j) {
      dataset.data[i][j] = dataset.data[i][j] / 255.0;
    }
  }
}

// Generate a random number between -0.05 and 0.05
float getRand() {
  return ((((float) rand()) / (float) RAND_MAX) - 0.5) / 10.0;
}

vector<float> split(const string &s, char delim) {
  stringstream ss(s);
  string item;
  vector<float> elems;
  float number;
  while (getline(ss, item, delim)) {
    number = atof(item.c_str());
    elems.push_back(number);
  }
  return elems;
}
