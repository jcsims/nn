#ifndef NET_H
#define NET_H

#define HIDDEN_UNITS 100
#define OUTPUT_UNITS 10
#define EPOCHS 100
#define LEARNING_RATE 0.1
#define THREADS 8
#define DATA_FILE "train_med.csv"
#define TEST_FILE "test_small.csv"

typedef struct {
  float* output;
  float* hiddenOutputs;
  float** hiddenWeights;
  float** outWeights;
  int dataDim;
  int hiddenUnits;
  int outputUnits;
} Net;

typedef struct {
  float** data;
  int dataDim;
  int observations;
} Dataset;

// Prototypes
// Read the data file passed, populate the number of observations and the
// dimension of the data
Dataset readData(std::string filename);
// Split a delimited line into integer tokens
std::vector<float> split(const std::string &s, char delim);
// Generate a random number between -0.05 and 0.05
float getRand();
// Initialize the network weights
Net initNet(int dataDim, int hiddenUnits, int outputUnits);
// Train a given network
// The data partition used to train depends on the thread number
void trainNet(Net &net, Dataset &dataset, int epochs, float learningRate, int id);
// Given a single input, calculate the output of the network
void calcOutput(float* input, Net &net);
// Given the combined inputs to a node, calculate the node's output
// This is a simple sigmoid (or logistic) function
float activationFunction(float in);
// Test the network on a given data file
void testNet(Net &net, std::string testFile);
// Scale the input data
// In this case, we'll tailor towards the digit recognizer
void scaleData(Dataset &dataset);
// When worker threads are done, average the work done by the different threads
void combineNets(Net* nets);

#endif
