% slides.tex
\documentclass[xcolor=x11names,compress]{beamer}

%% General document %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{graphicx}
\usepackage{tikz}
\usetikzlibrary{positioning, calc}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\tikzset{basic/.style={draw,fill=nodecolor!20,text width=1em,text badly centered}}
\tikzset{input/.style={basic,circle,node distance=3em}}
\tikzset{weights/.style={basic,rectangle}}
\tikzset{functions/.style={basic,circle,fill=nodecolor!10}}
\tikzset{every left delimiter/.style={xshift=0.7em,font=\scriptsize}}

%% Beamer Layout %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\useoutertheme[subsection=false,shadow]{miniframes}
\useinnertheme{default}
\usefonttheme{serif}
\usepackage{palatino}

\setbeamerfont{title like}{shape=\scshape}
\setbeamerfont{frametitle}{shape=\scshape}

\setbeamercolor*{lower separation line head}{bg=DeepSkyBlue4} 
\setbeamercolor*{normal text}{fg=black,bg=white} 
\setbeamercolor*{alerted text}{fg=red} 
\setbeamercolor*{example text}{fg=black} 
\setbeamercolor*{structure}{fg=black} 
 
\setbeamercolor*{palette tertiary}{fg=black,bg=black!10} 
\setbeamercolor*{palette quaternary}{fg=black,bg=black!10} 

\renewcommand{\(}{\begin{columns}}
\renewcommand{\)}{\end{columns}}
\newcommand{\<}[1]{\begin{column}{#1}}
\renewcommand{\>}{\end{column}}

\title{Artificial Neural Network}
\subtitle{with Backpropagation}
\author{Chris Sims}
\date{\today}

\begin{document}

\frame{\titlepage}

\begin{frame}{Outline}
  \tableofcontents
\end{frame}


%-------------ANN------------%
\section{The ANN}
\begin{frame}{The Perceptron}
  The perceptron is the basis of the neural network. It was originally
  conceived by Frank Rosenblatt in 1957 and can be described by this figure:

  \begin{center}
    \begin{tikzpicture}
      \node[functions] (center) {};
      \node[below of=center,font=\scriptsize,text width=4em] {activation function};
      \draw[thick] (0.5em,0.5em) -- (0,0.5em) -- (0,-0.5em) -- (-0.5em,-0.5em);
      \draw (0em,0.75em) -- (0em,-0.75em);
      \draw (0.75em,0em) -- (-0.75em,0em);
      \node[right of=center] (right) {$\hat{y}$};
          \path[draw,->] (center) -- (right);
      \node[functions,left=3em of center] (left) {$\sum$};
          \path[draw,->] (left) -- (center);
      \node[weights,left=3em of left] (2) {$w_2$} -- (2) node[input,left of=2] (l2) {$x_2$};
          \path[draw,->] (l2) -- (2);
          \path[draw,->] (2) -- (left);
      \node[below of=2] (dots) {$\vdots$} -- (dots) node[left of=dots] (ldots) {$\vdots$};
      \node[weights,below of=dots] (n) {$w_n$} -- (n) node[input,left of=n] (ln) {$x_n$};
          \path[draw,->] (ln) -- (n);
          \path[draw,->] (n) -- (left);
      \node[weights,above of=2] (1) {$w_1$} -- (1) node[input,left of=1] (l1) {$x_1$};
          \path[draw,->] (l1) -- (1);
          \path[draw,->] (1) -- (left);
      \node[weights,above of=1] (0) {$w_0$} -- (0) node[input,left of=0] (l0) {$1$};
          \path[draw,->] (l0) -- (0);
          \path[draw,->] (0) -- (left);
      \node[below of=ln,font=\scriptsize] {inputs};
      \node[below of=n,font=\scriptsize] {weights};
    \end{tikzpicture}
  \end{center}

\end{frame}

\begin{frame}{Making It Smarter}
  The single perceptron can only represent a linear decision surface. There are
  a few things that can make this model more powerful:

  \begin{itemize}
      \item Change the activation function
      \item Add layers to form a network or multi-layer perceptron (MLP)
  \end{itemize}
\end{frame}
\begin{frame}{An Example}
  Calculate a square root
  \centering
  \includegraphics[width=0.75\textwidth]{sample_net.pdf}
\end{frame}
\begin{frame}{What is an ANN?}

  There are many definitions for an Artificial Neural Network (ANN), but in
  general it has the following properties:

  \begin{itemize}
      \item Composed of layers of perceptrons -- many inputs, single output
      \item Multiple perceptron layers
        \begin{itemize}
          \item Input layer
          \item 0--n hidden layers (typically 1--2)
          \item Output layer
        \end{itemize}
      \item This implementation is a feed-forward network (input starts at the
        input nodes, and each node in a layer is connected to each node in the
        next layer)
  \end{itemize}

\end{frame}

\section{Implementation}

\begin{frame}{The Activation Function}
  Since the step activation function is so limited, a different function is
  typically used. Two common functions:

  \begin{itemize}
      \item Hyperbolic tan: $\tanh(x)$
      \item The logistic function: $\frac{1}{1 + e^{-x}}$
  \end{itemize}

  I used the logistic function. It has a simple derivative, which will make the
  learning process easier: 
  \[
    \frac{\mathrm d}{\mathrm d x} \, f(x) = f(x) (1 - f(x)),
  \]
  where $f(x) = \frac{1}{1 + e^{-x}}$.
\end{frame}

\begin{frame}{Backpropagation}
  In order to train the network, the errors made on each example are
  backpropagated from the output nodes through the hidden layer(s).

  \begin{align*}
    \delta_k &\leftarrow o_k (1 - o_k)(t_k - o_k) && \text{Compute error for
    each output unit} \\
    \delta_h &\leftarrow o_h (1- o_h) \sum_{k \in \text{outputs}}
    w_{kh}\delta_k && \text{Compute error for each hidden unit} \\
    w_{ji} &\leftarrow w_{ji} + \eta \delta_j x_{ji}
    && \text{Update each network weight}
  \end{align*}
  Where $w_{ji}$ is the weight from $i$ to $j$, $x_{ji}$ is the input from $i$
  to $j$, and $o_h$ is the output from a node.
\end{frame}

\section{Sequential}
\begin{frame}{Initial results}
  To start:
  \begin{itemize}
    \item 784 input nodes
    \item 10 hidden units
    \item 1 output node
  \end{itemize}

  Run time was fairly short (7.25 minutes), but accuracy was poor (error rate
  20\%). 

\end{frame}
\begin{frame}{Final Implementation}

  \begin{itemize}
    \item 784 input nodes
    \item 100 hidden units
    \item 10 output nodes
  \end{itemize}

  Run time increased to 40 minutes, error rate dropped to 3\%!

\end{frame}


\section{Parallel}
\begin{frame}{Parallel -- Threads}
  \begin{itemize}
    \item 784 input nodes
    \item 100 hidden units
    \item 10 output nodes
    \item 8 threads, 16 threads
  \end{itemize}

  Run time went down to around 14.7 minutes, but accuracy dropped to 6\%.
\end{frame}

\end{document}
